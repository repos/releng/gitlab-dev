# gitlab-dev

A GitLab developer environment that supports local gems and includes Zuul for
gating.

## WARNING (privileged container and other tomfoolery)

Wait!

Before you start up the environment, be aware that the `zuul-executor` service
needs to run in a privileged container so it can isolate Zuul/Ansible playbook
execution. You can read more about that in the [Zuul documentation on playbook
execution][zuul_executor_playbooks].

Even better, the `gitlab-runner` service mounts the host Docker socket so it
can operate as a Docker based GitLab CI executor. Processes within this
service container can do just about anything on the dockerd host.

## Quickstart

### Create the environment

```sh
docker compose up --build -d
```

You'll need to wait a while (2-3 minutes) for GitLab to fully initialize. See
all service progress with:

```sh
docker compose logs -f
```

### Use the environment

 - GitLab http://gitlab.local.wmftest.net:8084
   - `root`/`password` (global admin)
   - `dev`/`password` (a maintainer of `repos` and `repos/test-project`)
   - `zuul`/`password` (the Zuul service user)
 - Zuul http://zuul-web.local.wmftest.net:9000
   - Test Zuul integration by making changes to the [test project][test_project]
     using GitLab's IDE or via normal git workflows.
     - Comment "recheck" in MRs to (re)trigger the check pipeline
 - Add your SSH key to the Developer account using `./auth-ssh-key ~/.ssh/{key}.pub`
 - Look in [.env](./.env) for all usable "secrets" including API tokens

## Services

#### GitLab

 - Open http://gitlab.local.wmftest.net:8084
 - A test project is included at http://gitlab.local.wmftest.net:8084/repos/test-project
 - Local gems are loaded from the [gems](./gems) directory.
 - Personal access tokens have been provisioned for all users as well as
   project webhooks for Zuul integration.

#### GitLab Runner

 - The GitLab runner provided by the `gitlab-runner` service uses the docker
   executor to schedule jobs. Jobs are run on the same Docker network as the
   other services in the [docker-compose.yaml](./docker-compose.yaml) so they
   can access, among other things, the rsyncd daemon running on
   `zuul-executor`.

#### Zuul

Zuul has many services, but to perform basic monitoring and (re)configuration
of Zuul in this environment, you can:

 - Monitor all services with `docker compose logs -f`
 - View the Zuul dashboard at http://zuul-web.local.wmftest.net:9000/
 - Change Zuul jobs/pipelines/projects/nodesets by either:
   - Editing files directly under [repos/zuul-config](./repos/zuul-config) and
     running `./push-repos.sh` to update the GitLab repo.
   - Cloning http://gitlab.local.wmftest.net:8084/repos/zuul-config into a
     local directory and submitting changes to GitLab as an MR or direct push.
 - Change Zuul service config in [zuul/etc_zuul](./zuul/etc_zuul)
 - Either `docker compose --profile cmd up git-push-repos` 

## Inner workings

### Initial account/group/project setup

The very basic [gitlab_wmf_dev](./gems/gitlab_wmf_dev) gem includes [database
fixtures](./gems/gitlab_wmf_dev/db/fixtures) that create the Developer and
Zuul users and set personal access tokens for root/dev/zuul users.

The rest is done by the `gitlab-setup` service which makes use of [Zuul's
setup playbooks](./zuul/playbooks/setup.yaml). These were taken from Zuul
upstream's developer environment and refactored for GitLab.

### GitLab gem loading

See the included [Docker Compose configuration](./docker-compose.yaml) and
corresponding [Dockerfile](./Dockerfile) for details. In conjunction, they:

 - Install gems from our local [gems directory](./gems/) into the environment.
 - Install a `Gemfile.wmf` into the `gitlab-rails` service's working directory
   that:
   1. Loads upstream's `Gemfile`
   2. Defines an entry for each of our gems to be loaded as part of the
   default bundler group (Rails loads the default group in every environment).
 - Copy upstream's `Gemfile.lock` to `Gemfile.wmf.lock`
 - Execute `bundle lock` against `Gemfile.wmf` to update `Gemfile.wmf.lock`
   with entries for our gems. This is necessary because gems have been
   [frozen][frozen] in upstream's bundler config.
 - Add the following configuration to `/etc/gitlab/gitlab.rb` to ensure our
   `Gemfile.wmf` file is used when Rails loads gems.

```ruby
gitlab_rails['env'] = {
  "BUNDLE_GEMFILE" => "/opt/gitlab/embedded/service/gitlab-rails/Gemfile.wmf",
}
```

## License

gitlab-dev is licensed under the GNU General Public License 3.0 or later
(GPL-3.0+). See the LICENSE file for more details.

[frozen]: https://bundler.io/v1.12/man/bundle-config.1.html#LIST-OF-AVAILABLE-KEYS
[zuul_executor_playbooks]: https://zuul-ci.org/docs/zuul/4.6.0/discussion/components.html#trusted-and-untrusted-playbooks
[test_project]: http://gitlab.local.wmftest.net:8084/repos/test-project
