require_relative "lib/gitlab_wmf_dev/version"

Gem::Specification.new do |spec|
  spec.name        = "gitlab_wmf_dev"
  spec.version     = GitlabWmfDev::VERSION
  spec.authors     = ["Dan Duvall"]
  spec.email       = ["dduvall@wikimedia.org"]
  spec.summary     = "Summary of GitlabWmfDev."
  spec.description = "Description of GitlabWmfDev."
  spec.license     = "MIT"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 6.1"
end
