module GitlabWmfDev
  class Engine < ::Rails::Engine
    isolate_namespace GitlabWmfDev

    config.after_initialize do
      config.paths["db"].paths.each do |path|
        # add our db/fixtures that will execute upon container start
        SeedFu.fixture_paths << path.join("fixtures").expand_path.to_s
      end
    end
  end
end
