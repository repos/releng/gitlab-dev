root = User.find(1)
token = root.personal_access_tokens.find_or_initialize_by(name: "root")
token.scopes = ["api", "write_repository", "sudo"]
token.expires_at = 364.days.from_now
token.set_token(ENV["GITLAB_ROOT_TOKEN"])
token.save!
