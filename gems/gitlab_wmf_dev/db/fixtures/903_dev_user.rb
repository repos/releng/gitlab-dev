require "securerandom"

user = User.find_or_initialize_by(username: "dev")
user.update!(
  name: "Developer",
  email: "dev@gitlab.local.wmftest.net",
  confirmed_at: DateTime.now,
  password: SecureRandom.base64(20),
)
user.password = ENV["GITLAB_DEV_PASSWORD"]
user.save!(validate: false)

token = user.personal_access_tokens.find_or_initialize_by(name: "dev")
token.scopes = ["api", "write_repository"]
token.expires_at = 364.days.from_now
token.set_token(ENV["GITLAB_DEV_TOKEN"])
token.save!
