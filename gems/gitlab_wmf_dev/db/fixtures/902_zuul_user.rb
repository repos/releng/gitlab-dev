require "securerandom"

zuul = User.find_or_initialize_by(username: "zuul")
zuul.update!(
  name: "Zuul",
  email: "zuul@zuul.local.wmftest.net",
  confirmed_at: DateTime.now,
  password: SecureRandom.base64(20),
)
zuul.password = ENV["ZUUL_GITLAB_PASSWORD"]
zuul.save!(validate: false)

token = zuul.personal_access_tokens.find_or_initialize_by(name: "zuul")
token.scopes = ["api", "write_repository"]
token.expires_at = 364.days.from_now
token.set_token(ENV["ZUUL_GITLAB_TOKEN"])
token.save!
