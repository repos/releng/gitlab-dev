#!/bin/bash

git config --global init.defaultBranch main

mkdir -p ~/.ssh
chmod 0700 ~/.ssh
ssh-keyscan gitlab.local.wmftest.net >> ~/.ssh/known_hosts 2> /dev/null

for dir in $(find "$(dirname "${BASH_SOURCE[0]}")" -maxdepth 1 -mindepth 1 -type d); do
  echo "--- Updating $dir ---"
  repo="$(basename "$dir")"
  remote="ssh://git@gitlab.local.wmftest.net/repos/${repo}.git"
  cd "$(mktemp -d)"
  git init .
  git config user.name "Root"
  git config user.email "root@gitlab.local.wmftest.net"
  git remote add gitlab "ssh://git@gitlab.local.wmftest.net/repos/${repo}.git"
  git fetch gitlab
  git checkout main
  rsync -rlt "$dir"/ ./
  git add --all
  git commit -m 'Updated by repos/push-all.sh'
  git push -f
done
