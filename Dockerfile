# syntax=docker/dockerfile:1.6.0

ARG base=upstream
ARG upstream_version=16.5.0

# Using WMF based image and GitLab deb
FROM docker-registry.wikimedia.org/bullseye:20230814 AS wmf
RUN echo \
    "deb http://apt.wikimedia.org/wikimedia bullseye-wikimedia thirdparty/gitlab-bullseye" \
    > /etc/apt/sources.list.d/wikimedia-gitlab.list
RUN --mount=type=cache,target=/var/cache/apt \
    apt-get update && apt-get install -y \
        gitlab-ce \
        vim
COPY gems/ /opt/local/gitlab/gems/

# Using upstream GitLab image
FROM gitlab/gitlab-ce:${upstream_version}-ce.0 AS upstream
COPY gems/ /opt/local/gitlab/gems/

# Final dev image with hijacked Gemfile. Requires the following in gitlab.rb
#   gitlab_rails['env'] = {
#     "BUNDLE_GEMFILE" => "/opt/gitlab/embedded/service/gitlab-rails/Gemfile.wmf",
#   }
FROM $base AS dev
ARG BUNDLE_GEMFILE=/opt/gitlab/embedded/service/gitlab-rails/Gemfile.wmf

# We don't want password enforcement in a development environment
RUN echo '[]' > /opt/gitlab/embedded/service/gitlab-rails/config/weak_password_digests.yml

COPY Gemfile.wmf $BUNDLE_GEMFILE
RUN cp /opt/gitlab/embedded/service/gitlab-rails/Gemfile.lock "$BUNDLE_GEMFILE".lock
RUN cd "$(dirname "$BUNDLE_GEMFILE")" && \
    BUNDLE_IGNORE_CONFIG=1 bundle lock --local

FROM gitlab/gitlab-runner:v${upstream_version} AS runner
COPY runner-entrypoint /entrypoint
