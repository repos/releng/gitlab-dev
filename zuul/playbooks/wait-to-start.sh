#!/bin/bash

# Zuul needs to be able to connect to the remote systems in order to
# start.

wait_for_mysql() {
    echo `date -Iseconds` "Wait for mysql to start"
    for i in $(seq 1 120); do
        cat < /dev/null > /dev/tcp/mysql/3306 && return
        sleep 1
    done

    echo `date -Iseconds` "Timeout waiting for mysql"
    exit 1
}

wait_for_gitlab() {
    echo `date -Iseconds` "Wait for zuul SSH key to be added to GitLab"
    for i in $(seq 1 360); do
        [ $(curl -s -o /dev/null -H "Authorization: Bearer ${ZUUL_GITLAB_TOKEN}" -w "%{http_code}" http://gitlab.local.wmftest.net:8084/api/v4/user/keys) = "200" ] && return
        sleep 5
    done

    echo `date -Iseconds` "Timeout waiting for gitlab"
    exit 1
}

wait_for_mysql
wait_for_gitlab
